import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { MovieService } from '../_shared/services/movie.service';
import {
    ADD_MOVIES,
    ADD_MOVIES_SUCCESS,
    GET_MOVIES,
    GET_MOVIES_SUCCESS,
    movies,
    REMOVE_MOVIES,
    REMOVE_MOVIES_SUCCESS
} from '../reducers/movie';
import { map, switchMap } from 'rxjs/operators';


@Injectable()
export class MoviesEffects {
    constructor(private actions$: Actions,
                private movieService: MovieService) {
    }

    @Effect() getMovies$ = this.actions$.pipe(
        ofType(GET_MOVIES),
        switchMap(action =>
            this.movieService.getMovies().pipe(
                map(movie => ({ type: GET_MOVIES_SUCCESS, payload: movie })))));

    @Effect() addMovies$ = this.actions$.pipe(
        ofType(ADD_MOVIES),
        switchMap((action: any) =>
            this.movieService.addMovie(action.payload.title).pipe(
                map(movie => ({ type: ADD_MOVIES_SUCCESS, payload: movie })))));

    @Effect() removeMovies$ = this.actions$.pipe(
        ofType(REMOVE_MOVIES),
        switchMap((action: any) =>
            this.movieService.removeMovie(action.payload.data).pipe(
                map(movie => ({ type: REMOVE_MOVIES_SUCCESS, payload: movie })))));
}