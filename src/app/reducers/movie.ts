export const GET_MOVIES = 'GET_MOVIES';
export const REMOVE_MOVIES = 'REMOVE_MOVIES';
export const GET_MOVIES_SUCCESS = 'GET_MOVIES_SUCCESS';

export const ADD_MOVIES = 'ADD_MOVIES';
export const ADD_MOVIES_SUCCESS = 'ADD_MOVIES_SUCCESS';
export const REMOVE_MOVIES_SUCCESS = 'REMOVE_MOVIES_SUCCESS';

export function getMovies() {
    return {
        type: GET_MOVIES
    };
}

const initialState = {
    data: [],
    pending: false,
    error: null
};

export function movies(state = initialState, { type, payload }) {
    switch (type) {
        case GET_MOVIES:
            return Object.assign({}, state, { pending: true, error: null });
        case GET_MOVIES_SUCCESS:
            return Object.assign({}, state, { data: payload, pending: false });
        case ADD_MOVIES_SUCCESS:
            return Object.assign({}, state, {
                data: [ ...state.data, payload ]
            });
        case REMOVE_MOVIES_SUCCESS:
            return Object.assign({}, state, {
                data: [ ...payload ]
            });
        default:
            return state;
    }
}

export function addMovie(data) {
    return {
        type: ADD_MOVIES,
        payload: {
            data,
        }
    };
}
export function removeMovie(data) {
    return {
        type: REMOVE_MOVIES,
        payload: {
            data,
        }
    };
}