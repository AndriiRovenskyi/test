import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { addMovie } from '../../reducers/movie';

@Component({
    selector: 'app-movies',
    templateUrl: './movies.component.html',
    styleUrls: [ './movies.component.scss' ]
})
export class MoviesComponent implements OnInit {

    @Input() movies;
    @Output() removeMovie: EventEmitter<any>;

    constructor() {
        this.removeMovie = new EventEmitter<any>();
    }

    ngOnInit() {
    }


}
