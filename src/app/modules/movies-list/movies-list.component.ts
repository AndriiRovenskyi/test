import { Component, OnInit } from '@angular/core';
import { Movie } from '../../_shared/models/movie';
import { MovieService } from '../../_shared/services/movie.service';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material';
import { AddMovieComponent } from '../../_shared/dialogs/add-movie/add-movie.component';
import { ClickEvent, RatingChangeEvent } from 'angular-star-rating';
import { Store } from '@ngrx/store';
import { ADD_MOVIES, ADD_MOVIES_SUCCESS, addMovie, getMovies, removeMovie } from '../../reducers/movie';
import { MoviesEffects } from '../../effects/movie';

@Component({
    selector: 'app-movies-list',
    templateUrl: './movies-list.component.html',
    styleUrls: [ './movies-list.component.scss' ]
})
export class MoviesListComponent implements OnInit {
    public movies: Array<Movie>;
    public movies$: Observable<Array<Movie>>;
    public onClickResult: ClickEvent;
    public onRatingChangeResult: RatingChangeEvent;
    public addMovieSuccess$: Observable<any>;

    constructor(private movieService: MovieService, public dialog: MatDialog, private store: Store<any>, private moviesEffects: MoviesEffects) {
        this.store.dispatch(getMovies());
        this.movies$ = store.select('movies');
        this.addMovieSuccess$ = this.moviesEffects.addMovies$;
    }

    ngOnInit() {
    }

    removeMovie(id): void {
        this.store.dispatch(removeMovie(id));
    }

    addMovie(): void {
        const dialogRef = this.dialog.open(AddMovieComponent);
        dialogRef.afterClosed().subscribe((res: any) => {
            if (!res) return;
            this.store.dispatch(addMovie(res));
        });
    }

    onClick($event: ClickEvent): void {
        this.onClickResult = $event;
    }

    onRatingChange($event: RatingChangeEvent, i): void {
        this.movies[ i ].rating = $event.rating;
        this.movieService.updateMovies(this.movies);
        this.onRatingChangeResult = $event;
    }


}
