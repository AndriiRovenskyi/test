import { Injectable } from '@angular/core';
import { Movies } from '../../mocks/movies';
import { Movie } from '../models/movie';
import { BehaviorSubject, interval, Observable, timer } from 'rxjs';
import { mapTo } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class MovieService {
    public movies: BehaviorSubject<Array<Movie>>;

    constructor() {
        this.movies = new BehaviorSubject(Movies.moviesList);
    }

    getMovies(): Observable<Array<Movie>> {
        return this.movies.asObservable();
    }

    addMovie(data) {
        const source = timer(2000);
        return source.pipe(mapTo(data));
    }

    removeMovie(id) {
        const source = timer(2000);
        const x = this.movies.getValue().filter(el => el.id != id);
        debugger
        return source.pipe(mapTo(x));
    }

    updateMovies(arr): void {
        this.movies.next(arr);
    }
}
